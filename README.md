# Entertainment DB

## REST API
Sovelluksen palvelinpuolta on tähän mennessä testattu ajamalla se paikallisesti portissa 8081. Näin ajettuna jokaisen sovelluksen osan osoitteen alussa lukee ```http://localhost:8081```, kyselyt ollen muotoa ```http://localhost:8081/api/[kysely]```.

Alla olevissa ohjeissa osoitteen alku on jätetty pois ja ne alkavat kohdasta ```/api```.

Ohjeisiin on myös merkitty sulkuihin kyseisen pyynnön onnistumiseen vaadittu metodi.

HUOM: Palvelin vastaa ainoastaan osoitteesta ```http://localhost:8080``` tuleviin kyselyihin.

### Montaa taulua koskevat haut

#### Hae kaikki
Koskee tauluja ```entertainment```, ```genres```, ```jobs```, ```people```, ```types``` ja ```works```.

Muoto on seuraavanlainen:
```
(GET) /api/[taulun nimi]
```
Vastauksena saat kaikki ko. taulun tietueet (ja tietyissä tapauksissa niihin liitettyä tietoa).

Esimerkiksi
```
(GET) /api/people
```

#### Hae tietty
Koskee tauluja ```entertainment```, ```genres```, ```people```, ```types``` ja ```works```.
```
(GET) /api/[taulun nimi]/[id]
```
Vaustauksena saat kyseisen taulusta tietueen, jonka ```id```-kentän arvo on sama kuin kyselyn lopussa.

Esimerkiksi
```
(GET) /api/genres/8
```

#### Order
Koskee tauluja ```entertainment```, ```genres```, ```people```, ```types``` ja ```works```.
```
(GET) /api/[taulun nimi]?order=[taulun ensimmäinen kirjain].[kentän nimi]
```
```order```-kyselyvalinnalla voit valita missä järjestyksessä tietueet annetaan.

Esimerkiksi
```
(GET) /api/entertainment?order=e.released
```
palauttaa ```entertainment```-taulun tietueet ```released```-kentän arvojen kasvavassa järjestyksessä.

Voit myös hakea tietueet laskevassa järjestyksessä asettamalla perään ```%20desc```.

Esimerkiksi
```
(GET) /api/entertainment?order=e.released%20desc
```
palauttaa samat tietueet kuin edellinen esimerkki, mutta päinvastaisessa järjestyksessä.

#### Like
Koskee tauluja ```entertainment``` ja ```people```.

Kummallakin edellä mainitulla taululla on kenttä ```name```. Kyselyvalinnalla ```like``` saat vastauksena vain ne tietueet, joissa ```name``` sisältää antamasi tekstin pätkän.

Esimerkiksi
```
(GET) /api/entertainment?like=plan
```
Palauttaa tekstin "plan" sisältävät nimikkeet, kuten "Planet of the Apes" ja "Airplane!".

### Entertainment (GET)
Seuraavat kyselyvalinnat koskevat ainoastaan otsikon taulua.

#### Genres
Voit antaa ```genres```-kyselyvalinnalle arvoksi yhden luvun tai useamman luvun pilkulla eroteltuna. Lukujen tulee vastata ```genres```-taulun ```id```-kentän arvoja.

Esimerkiksi
```
(GET) /api/entertainment?genres=1
```
antaa sellaiset nimikkeet, jotka on liitetty genreen "Comedy" (jonka ```id``` on 1) ja
```
(GET) /api/entertainment?genres=11,15
```
antaa nimikkeet, jotka kuuluvat genreen "Adventure" (11) tai "Family" (15).

#### No genres
Sisällyttämällä kyselyvalinnan ```no_genres``` ja antamalla sille arvoksi ```true``` saat vastauksena sellaiset nimikkeet, joita ei ole liitetty yhteenkään genreen.

#### Types
Lukujen tulee vastata taulun ```types``` kentän ```id``` arvoja.

Esimerkiksi
```
(GET) /api/entertainment?types=4,10
```
palauttaa "Video Game" tai "TV Series" -kategoriaan kuuluvat nimikkeet.

#### Earliest ja latest
Voit rajata nimikkeitä ilmestymisvuoden mukaan.

Esimerkiksi
```
(GET) /api/entertainment?earliest=1980
```
palauttaa vuonna 1980 tai myöhemmin ilmestyneet nimikkeet.
```
(GET) /api/entertainment?latest=1999
```
palauttaa vuonna 1999 tai aikaisemmin ilmestyneet nimikkeet.

#### Budget at least ja budget at most
Voit rajata nimikkeitä budjetin suuruuden mukaan.

Esimerkiksi
```
(GET) /api/entertainment?budget_at_least=10000000&budget_at_most=50000000
```
palauttaa sellaiset nimikkeet, joiden ```budget```-kentän arvo on välillä 10 miljoonaa ja 50 miljoonaa dollaria.

#### Gross at least ja gross at most
```gross_at_least``` ja ```gross_at_most``` toimivat pitkälty samoin kuin edellisen kohdan kyselyvalinnat, mutta koskevat nimikkeen tuottoja eli ```gross```-kentän arvoja.

### Genres (GET)
Alla ```genres```-taulua koskevat kyselyvalinnat.

#### Title id
Esimerkiksi
```
(GET) /api/genres?title_id=3
```
antaa ne genret, joihin nimike "Snow White and the Seven Dwarfs" (3) on liitetty.

HUOM: ```title_id```-kyselyvalinnalle voi antaa vain yhden luvun.

### People (GET)

#### Jobs
Kyselyvalinnalla ```jobs``` voi rajata haettavia henkilöitä ammattien mukaan.

Esimerkiksi
```
(GET) /api/people?jobs=2,5
```
palauttaa henkilöt, jotka ovat työskennelleet ohjaajana (2) tai tuottajana (5).

#### No jobs
```
(GET) /api/people?no_jobs=true
```
palauttaa henkilöt, joille ei ole merkitty (vielä) yhtäkään työtä.

### Works (GET)

#### Acting
```
(GET) /api/works?acting=true
```
palauttaa sellaiset työt, joissa työtehtävänä on näytteleminen, kun taas
```
(GET) /api/works?acting=false
```
palauttaa kaikki muut työt.

#### Person id
```
(GET) /api/works?person_id=[id]
```
Antaa tuloksena vain kyseiseen henkilöön liittyvät työt.

#### Title id
```title_id``` toimii vastaavasti kuin ```person_id```. Rajaa tuloksena saatuja töitä nimikkeen perusteella.

### Lisääminen
Lisääminen tapahtuu ```POST```-pyynnön avulla. Koskee tauluja ```entertainment```, ```jobs```, ```related_genres```, ```roles``` ja ```works```.
```
(POST) /api/[taulun nimi]
```
Pyynnön vartalon (body) tulee sisältää taulun tietueita vastaava, JSON-muotoon kirjoitettu olio. Esim. ```people```-tauluun lisättäessä täytyy käyttää oliota, jolla on avaimina ```name```, ```born``` ja ```died``` ja niiden arvoina tauluun soveltuvat tiedot – tässä tapauksessa merkkijono ja kaksi [päivämääräksi soveltuvaa merkkijonoa](https://www.w3schools.com/js/js_date_formats.asp). Avaimille ```born``` ja ```died``` on mahdollista antaa arvoksi ```null```. Taulun kenttää ```known_for``` vastaavaa avainta ei tarvitse oliolle antaa, koska se viittaa ```works```-tauluun eikä henkilön töitä ei voi olla tietokannassa ennen henkilön itsensä lisäämistä.

### Päivittäminen
Saatavilla ainoastaan ```people```-taululle. Kuten lisäämisessä, pyynnön bodyn tulee sisältää tietuetta vastaava olio. Pyynnön metodin tulee olla ```PUT```.
```
(PUT) /api/people/[muokatun henkilön id]
```
Sovelluksessa ```people```-taulun muokkaaminen on toteutettu, jotta tietokantaan voisi viedä henkilön ```known_for```-kenttään asetettavaa tietoa. Edellisessä kohdassa (Lisääminen) kerrotaan, miksei kyseiselle kentälle voi antaa arvoa vielä henkilöä lisättäessä.

### Palautettavat tiedot
Useimpiin tauluihin kohdistuvat kyselyt palauttavat myös tietueisiin liittyviä tietoja muista tauluista. Alla kuvataan tarkemmin, minkä muotoisia vastauksia kuhunkin tauluun kohdistuvat ```GET```-pyynnöt tuottavat.

#### entertainment
```
SELECT e.id, e.name, e.released, e.ended, e.budget, e.gross, e.type_id, t.name AS type
FROM entertainment e
JOIN types t
ON e.type_id = t.id
```

#### genres
```
SELECT *
FROM genres
```

#### jobs
```
SELECT *
FROM jobs
```

#### people
```
SELECT p.id id, p.name name, p.born, p.died, p.known_for, j.name AS job, e.name AS title
FROM people p
LEFT JOIN works w
ON p.known_for = w.id
LEFT JOIN jobs j
ON w.job_id = j.id
LEFT JOIN entertainment e
ON w.title_id = e.id
```

#### types
```
SELECT *
FROM types
```

#### works
```
SELECT w.id, w.person_id, w.title_id, w.job_id, p.name, e.name AS title, j.name AS job, e.released, e.ended, t.name AS type, r.name AS role
FROM works w
JOIN people p
ON w.person_id = p.id
JOIN entertainment e
ON w.title_id = e.id
JOIN jobs j
ON w.job_id = j.id
JOIN types t
ON e.type_id = t.id
LEFT JOIN roles r
ON r.work_id = w.id
```

## Tietokanta

### Käyttöönotto
Projektin alahakemistosta ```entertainment_db > src > assets``` löytyy tiedosto ```seed.sql```. Se sisältää SQL-komennot sovelluksen tietokannan ja taulujen luomiseen. Se myös täyttää tauluihin valmiiksi dataa sovelluksen kokeilemista varten.

HUOM: Jos palvelimeltasi löytyy jo tietokanta nimeltä ```web-sovelluskehitys```, tiedoston sisältö päällekirjoittaa sen. 

### Tietokantataulut

#### entertainment
```
id          INT         NOT NULL    auto_increment
name        VARCHAR     NOT NULL
released    YEAR        NOT NULL
ended       YEAR        NULL
budget      INT         NULL
gross       BIGINT      NULL
type_id     INT         NOT NULL
```
```id``` on pääavain. Vierasavain ```type_id``` viittaa tauluun ```types```.

#### genres
```
id      INT         NOT NULL    auto_increment
name    VARCHAR     NOT NULL
```

#### jobs
```
id      INT         NOT NULL    auto_increment
name    VARCHAR     NOT NULL
```

#### people
```
id          INT         NOT NULL    auto_increment
name        VARCHAR     NOT NULL
born        DATE        NULL
died        DATE        NULL
known_for   INT         NULL
```
Vierasavain ```known_for``` viittaa tauluun ```works```.

#### related_genres
```
title_id    INT     NOT NULL
genre_id    INT     NOT NULL
```
Vierasavain ```title_id``` viittaa tauluun ```entertainment```. Vierasavain ```genre_id``` viittaa tauluun ```genres```.

#### roles
```
name        VARCHAR     NOT NULL
work_id     INT         NOT NULL
```
Vierasavain ```work_id``` viittaa tauluun ```works```.

#### types
```
id      INT         NOT NULL    auto_increment
name    VARCHAR     NOT NULL
```

#### works
```
id          INT     NOT NULL    auto_increment
person_id   INT     NOT NULL
title_id    INT     NOT NULL
job_id      INT     NOT NULL
```
```person_id``` viittaa tauluun ```people```, ```title_id``` viittaa tauluun ```entertainment``` ja ```job_id``` viittaa tauluun ```jobs```.

## Project setup
```
npm install express
npm install mysql
npm install body-parser
npm install vue
npm install bootstrap
npm install cors
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
