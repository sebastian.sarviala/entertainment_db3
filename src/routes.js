import Insert from "@/components/Insert";
import Person from "@/components/Person";
import PersonEdit from "@/components/PersonEdit";
import Search from "@/components/Search";
import Splash from "@/components/Splash";
import Title from "@/components/Title";
import TitleEdit from "@/components/TitleEdit";

const routes = [
    { path: '/', name: 'Splash', component: Splash },
    { path: '/search', name: 'Search', component: Search },
    { path: '/insert', name: 'Insert', component: Insert },
    { path: '/entertainment/:id/edit', name: 'TitleEdit', component: TitleEdit },
    { path: '/entertainment/:id', name: 'Title', component: Title },
    { path: '/people/:id/edit', name: 'PersonEdit', component: PersonEdit },
    { path: '/people/:id', name: 'Person', component: Person },
];

export default routes;