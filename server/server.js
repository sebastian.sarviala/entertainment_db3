const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const mysql = require('mysql');
const app = express();
const con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'web-sovelluskehitys'
});
const utils = ((con) => {

    const clean = (arg, accepted_values) => {
        arg = '' + arg;
        arg = arg.replace('%20', ' ').toLowerCase();
        accepted_values = accepted_values.split('');
        arg = arg.toString().split('');
        arg = arg.filter(
            c => accepted_values.indexOf(c) !== -1
        );
        return arg.join('');
    };

    /**
     * Tells whether there was an error. Displays appropriate information.
     *
     * @param err
     * @param msg
     * @returns {boolean}
     */
    const handleError = (err, msg) => {
        if (err) {
            console.error(err);
        } else {
            console.log(msg);
        }

        return !err;
    };

    const initial = table => {
        return table.substr(0, 1).toLowerCase()
    };

    const numerify = arg => {
        arg = clean(arg, '0123456789');
        return parseInt(arg);
    };

    const prepend = params => {
        if (typeof params === 'string' && params.length === 0) {
            return ' WHERE ';
        } else {
            return ' AND ';
        }
    };

    const queryArray = arg => {
        arg = clean(arg, '0123456789,');
        return `(${arg})`;
    };

    const queryOrder = arg => {
        return clean(arg, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ._ ');
    };

    /**
     * Transforms request arguments into a nice string.
     *
     * @param args
     * @returns {string}
     */
    const queryString = (args) => {
        let result = '';
        const keys = Object.keys(args);
        if (keys.length > 0) {
            for (let key of keys) {
                result += `${result ? '&' : '?'}${key}=${args[key]}`;
            }
            return result;
        } else {
            return '';
        }
    };

    return {
        doQuery(res, method, path, sql, args, msg) {
            if (typeof res === 'object' && typeof args === 'object' && typeof method === 'string' && typeof sql === 'string') {
                con.query(sql, Object.values(args), (err, result) => {
                    if (handleError(err,
                        msg ? `${method} ${path}${path.charAt(path.length - 1) === '/' ?
                            args.id : queryString(args)} successful` : result)) {
                        res.send(result);
                    }
                })
            } else {
                console.error('Faulty arguments in doQuery')
            }
        },
        paramsAndArgs(query, table) {
            let where = '';
            let order_by = '';
            const first_args = {};
            const last_args = {};

            if (query.order) {
                const order = queryOrder(query.order).trim().split(' ');
                order_by += ` ${order[0]} ${order[order.length - 1].toUpperCase() === 'DESC' ? 'DESC' : ''}, `;
                last_args.order = order;
            }

            if ((table === 'people' || table === 'entertainment') && query.like) {
                const like = query.like.replace('%20', ' ');
                where += `${prepend(where)} ${initial(table)}.name LIKE ? `;
                order_by += `${initial(table)}.name LIKE ? DESC, `;
                first_args.like1 = `%${like}%`;
                last_args.like2 = `${like}%`;
            }

            if ((query.jobs || query.no_jobs) && table === 'people') {
                const jobs = query.jobs ? queryArray(query.jobs) : null;
                const none = !!query.no_jobs;
                if (jobs) {
                    where += `
                        ${prepend(where)} ${none ? '(' : ''} p.id IN (
                            SELECT w2.person_id
                            FROM works w2
                            WHERE w2.job_id IN ${jobs}
                        ) `;

                    last_args.jobs = jobs;
                }
                if (none) {
                    where += `
                        ${jobs ? 'OR' : prepend(where)} NOT EXISTS(
                            SELECT *
                            FROM works w3
                            WHERE w3.person_id = p.id
                        )${jobs ? ')' : ''}`;

                    last_args.no_jobs = none;
                }
            }

            if ((query.genres || query.no_genres) && table === 'entertainment') {
                const genres = query.genres ? queryArray(query.genres) : null;
                const none = !!query.no_genres;
                if (genres) {
                    where += `
                        ${prepend(where)} ${none ? '(' : ''} e.id IN (
                            SELECT r.title_id
                            FROM related_genres r
                            WHERE r.genre_id IN ${genres}
                        ) `;

                    last_args.genres = genres;
                }
                if (none) {
                    where += `
                        ${genres ? 'OR' : prepend(where)} NOT EXISTS(
                            SELECT *
                            FROM related_genres r2
                            WHERE r2.title_id = e.id
                        )${genres ? ')' : ''}`;

                    last_args.no_genres = none;
                }
            }

            if (query.types && table === 'entertainment') {
                const types = queryArray(query.types);
                where += `${prepend(where)} t.id IN ${types} `;
                last_args.types = types;
            }

            if ((query.earliest || query.latest) && table === 'entertainment') {
                where += `
                    ${prepend(where)} released BETWEEN ${query.earliest ? '?' : 1900} 
                    AND ${query.latest ? '?' : 2099} `;

                if (query.earliest) {
                    first_args.earliest = numerify(query.earliest);
                }

                if (query.latest) {
                    first_args.latest = numerify(query.latest);
                }
            }

            if ((query.budget_at_least || query.budget_at_most) && table === 'entertainment') {
                where += `
                    ${prepend(where)} budget BETWEEN ${query.budget_at_least ? '?' : 0} 
                    AND ${query.budget_at_most ? '?' : 500 * 1000 * 1000} `;

                if (query.budget_at_least) {
                    first_args.budget_at_least = numerify(query.budget_at_least);
                }

                if (query.budget_at_most) {
                    first_args.budget_at_most = numerify(query.budget_at_most);
                }
            }

            if ((query.gross_at_least || query.gross_at_most) && table === 'entertainment') {
                where += `
                    ${prepend(where)} gross BETWEEN ${query.gross_at_least ? '?' : 0} 
                    AND ${query.gross_at_most ? '?' : 5000 * 1000 * 1000} `;

                if (query.gross_at_least) {
                    first_args.gross_at_least = numerify(query.gross_at_least);
                }

                if (query.gross_at_most) {
                    first_args.gross_at_most = numerify(query.gross_at_most);
                }
            }

            if (query.title_id && table === 'genres') {
                const title_id = numerify(query.title_id);
                where += `
                    ${prepend(where)} id IN (
                        SELECT rg.genre_id
                        FROM related_genres rg
                        WHERE rg.title_id = ?
                    )`;

                first_args.title_id = title_id;
            }

            if (query.acting && table === 'works') {
                const acting = query.acting;
                if (acting === 'true') {
                    where += `
                        ${prepend(where)} j.id = 1`;
                } else if (acting === 'false') {
                    where += `
                        ${prepend(where)} j.id != 1`;
                }

                last_args.acting = acting;
            }

            if (query.person_id && table === 'works') {
                const person_id = numerify(query.person_id);
                where += `
                    ${prepend(where)} w.person_id = ?`;

                first_args.person_id = person_id;
            }

            if (query.title_id && table === 'works') {
                const title_id = numerify(query.title_id);
                where += `
                    ${prepend(where)} w.title_id = ?`;

                first_args.title_id = title_id;
            }

            const args = {...first_args, ...last_args};

            return [where, args, order_by];
        },
    }
})(con);

con.connect(err => {
    if (err)
        console.log('connect failed');
    else
        console.log('connect successful');
});

app.use(cors({
    origin: 'http://localhost:8080'
}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// GETS

app.get('/api/entertainment/:id', (req, res) => {
    if (req.params.id) {
        const id = req.params.id;
        const sql = `SELECT e.id, e.name, e.released, e.ended, e.budget, e.gross, e.type_id, t.name type
            FROM entertainment e
            JOIN types t
            ON e.type_id = t.id
            WHERE e.id = ?`;

        utils.doQuery(res, 'GET', '/api/entertainment/', sql, {id: id}, true)
    } else {
        console.error('Invalid id');
    }
});

app.get('/api/entertainment', (req, res) => {
    const [where, args, order_by] = utils.paramsAndArgs(req.query, 'entertainment');
    const sql = `SELECT e.id, e.name, e.released, e.ended, e.budget, e.gross, e.type_id, t.name type
        FROM entertainment e
        JOIN types t
        ON e.type_id = t.id
        ${where}
        ORDER BY ${order_by} e.name, e.released DESC`;

    utils.doQuery(res, 'GET', '/api/entertainment', sql, args, true)
});

app.get('/api/genres/:id', (req, res) => {
    if (req.params.id) {
        const id = req.params.id;
        const sql = `SELECT *
            FROM genres
            WHERE id = ?`;

        utils.doQuery(res, 'GET', '/api/genres/', sql, {id: id}, true)
    } else {
        console.error('Invalid id');
    }
});

app.get('/api/genres', (req, res) => {
    const [where, args, order_by] = utils.paramsAndArgs(req.query, 'genres');
    const sql = `SELECT *
            FROM genres
            ${where}
            ORDER BY ${order_by} name`;

    utils.doQuery(res, 'GET', '/api/genres', sql, args, true)
});

app.get('/api/jobs', (req, res) => {
    const [where, args, order_by] = utils.paramsAndArgs(req.query, 'jobs');
    const sql = `
        SELECT *
        FROM jobs
        ${where}
        ORDER BY ${order_by} name`;

    utils.doQuery(res, 'GET', '/api/jobs', sql, args, true)
});

app.get('/api/people/:id', (req, res) => {
    if (req.params.id) {
        const id = req.params.id;
        const sql = `SELECT p.id id, p.name name, p.born, p.died, p.known_for, j.name job, e.name title
            FROM people p
            LEFT JOIN works w
            ON p.known_for = w.id
            LEFT JOIN jobs j
            ON w.job_id = j.id
            LEFT JOIN entertainment e
            ON w.title_id = e.id
            WHERE p.id = ?`;

        utils.doQuery(res, 'GET', '/api/people/', sql, {id: id}, true)
    } else {
        console.error('invalid id');
    }
});

app.get('/api/people', (req, res) => {
    const [where, args, order_by] = utils.paramsAndArgs(req.query, 'people');

    const sql = `SELECT p.id id, p.name name, p.born, p.died, p.known_for, j.name job, e.name title
            FROM people p
            LEFT JOIN works w
            ON p.known_for = w.id
            LEFT JOIN jobs j
            ON w.job_id = j.id
            LEFT JOIN entertainment e
            ON w.title_id = e.id
            ${where}
            ORDER BY ${order_by} p.name, p.died = null DESC, p.born DESC`;

    utils.doQuery(res, 'GET', '/api/people', sql, args, true)
});

app.get('/api/types/:id', (req, res) => {
    if (req.params.id) {
        const id = req.params.id;
        const sql = `SELECT *
            FROM types
            WHERE id = ?`;

        utils.doQuery(res, 'GET', '/api/types/', sql, {id: id}, true)
    } else {
        console.error('Invalid id');
    }
});

app.get('/api/types', (req, res) => {
    const [where, args, order_by] = utils.paramsAndArgs(req.query, 'types');
    const sql = `SELECT *
        FROM types
        ${where}
        ORDER BY ${order_by} name`;

    utils.doQuery(res, 'GET', '/api/types', sql, args, true)
});

app.get('/api/works/:id', (req, res) => {
    if (req.params.id) {
        const id = req.params.id;
        const sql = `SELECT w.id, w.person_id, w.title_id, w.job_id, p.name name, e.name title, j.name job
            FROM entertainment e, jobs j, works w
            WHERE w.id = ?
            AND title_id = e.id
            AND job_id = j.id`;

        utils.doQuery(res, 'GET', '/api/works/', sql, {id: id}, false)
    } else {
        console.error('Invalid id');
    }
});

app.get('/api/works', (req, res) => {
    const [where, args, order_by] = utils.paramsAndArgs(req.query, 'works');
    const sql = `
        SELECT w.id, w.person_id, w.title_id, w.job_id, p.name, e.name title, j.name job, e.released, e.ended, t.name type, r.name role
        FROM works w
        JOIN people p
        ON w.person_id = p.id
        JOIN entertainment e
        ON w.title_id = e.id
        JOIN jobs j
        ON w.job_id = j.id
        JOIN types t
        ON e.type_id = t.id
        LEFT JOIN roles r
        ON r.work_id = w.id
        ${where}
        ORDER BY ${order_by} p.name, e.name, j.name`;

    utils.doQuery(res, 'GET', '/api/works', sql, args, true)
});

// POSTS

app.post('/api/entertainment', (req, res) => {
    const args = {};
    const sql = `
        INSERT INTO entertainment (name, released, ended, budget, gross, type_id)
        VALUES (?, ?, ?, ?, ?, ?)`;

    args.name = req.body.name;
    args.released = req.body.released;
    args.ended = req.body.ended;
    args.budget = req.body.budget;
    args.gross = req.body.gross;
    args.type_id = req.body.type_id;

    utils.doQuery(res, 'POST', '/api/entertainment', sql, args, true)
});

app.post('/api/people', (req, res) => {
    const args = {};
    const sql = `
        INSERT INTO people (name, born, died)
        VALUES (?, ?, ?)`;

    args.name = req.body.name;
    args.born = req.body.born;
    args.died = req.body.died;

    utils.doQuery(res, 'POST', '/api/people', sql, args, true)
});

app.post('/api/related_genres', (req, res) => {
    const args = {};
    const sql = `
        INSERT INTO related_genres (title_id, genre_id)
        VALUES (?, ?)`;

    args.title_id = req.body.title_id;
    args.genre_id = req.body.genre_id;

    utils.doQuery(res, 'POST', '/api/related_genres', sql, args, true)
});

app.post('/api/roles', (req, res) => {
    const args = {};
    const sql = `
        INSERT INTO roles (name, work_id)
        VALUES (?, ?)`;

    args.name = req.body.name;
    args.work_id = req.body.work_id;

    utils.doQuery(res, 'POST', '/api/roles', sql, args, true)
});

app.post('/api/works', (req, res) => {
    const args = {};
    const sql = `
        INSERT INTO works (person_id, title_id, job_id)
        VALUES (?, ?, ?)`;

    args.person_id = req.body.person_id;
    args.title_id = req.body.title_id;
    args.job_id = req.body.job_id;

    utils.doQuery(res, 'POST', '/api/works', sql, args, true)
});

// PUTS

app.put('/api/people/:id', (req, res) => {
    if (req.params.id) {
        const args = {};
        const sql = `
            UPDATE people
            SET name = ?, born = ?, died = ?, known_for = ?
            WHERE id = ?`;

        args.name = req.body.name;
        args.born = req.body.born;
        args.died = req.body.died;
        args.known_for = req.body.known_for;
        args.id = req.params.id;

        utils.doQuery(res, 'PUT', '/api/people/', sql, args, true)
    } else {
        console.error('invalid id');
    }
});

const server = app.listen(8081, () => {
    const port = server.address().port;

    console.log('Server listening at http://localhost:' + port);
});